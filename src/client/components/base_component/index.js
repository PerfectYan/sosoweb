import React from 'react';
import qs from 'qs';

const __IS_BROWSER__ = typeof window === 'object' && window.window === window;

export default class BaseComponent extends React.Component {

    constructor(props) {
        super(props);

        let prefetchData = null;
        if (__IS_BROWSER__) {
            prefetchData = window.__PREFETCH_DATA__;
            delete window.__PREFETCH_DATA__;
        } else {
            prefetchData = this.props.staticContext.prefetchData;
        }

        this.state = {
            prefetchData
        }
    }

    shouldPrefetch() {
        return typeof this.prefetch === 'function' && !this.state.prefetchData
    }

    componentDidMount() {
        if (this.shouldPrefetch()) {
            const query = qs.parse(this.props.location.search.replace(/^\?/, ''));
            this.prefetch(query).then(data => {
                this.setState({ prefetchData: data });
            })
        }
    }

    render() {
        if (this.shouldPrefetch()) {
            return <div>Loading ...</div>
        }

        return this.renderContent();
    }

    renderContent() {
        throw new Error('子组件必须实现renderContent方法');
    }
}