import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss';

export default class Footer extends React.Component {
    render() {
        return (
            <div className="blue" styleName="red">
                <Link to="/">Home</Link> <br/>
                <Link to="/about">About</Link><br/>
                <Link to="/me">Me</Link><br/>
                <Link to="/login">Login</Link>
            </div>
        );
    }
}