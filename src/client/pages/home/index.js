import React from 'react';
import { Link } from 'react-router-dom';
import BaseComponent from '../../components/base_component';
import { getProjects, getIssues } from '../../../share/api';

export default class Home extends BaseComponent {
    prefetch(query) {
        return Promise.all([getProjects(), getIssues()]);
    }

    renderContent() {
        const { prefetchData } = this.state;
        const [ projectsData, issuesData ] = prefetchData;

        return (
            <div>
                Home Page <Link to="/about">About</Link>
                <div>{prefetchData.name}</div>

                <h1>issues</h1>
                {
                    issuesData.data.issues.map(item => <div key={item.id}>{item.subject}</div>)
                }

                <h1>projects</h1>
                {
                    projectsData.data.projects.map(item => <div key={item.id}>{item.name}</div>)
                }
            </div>
        )
    }
}