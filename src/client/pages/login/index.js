import React from 'react';
import BaseComponent from '../../components/base_component';
import qs from 'qs';
import { login, logout, isLogin } from '../../../utils/user';

export default class Login extends BaseComponent {

    renderContent() {
        return (
            <div>welcome to Login Page

                {!isLogin() ?
                    <button onClick={() => login().then(() => {
                            this.props.history.push(decodeURIComponent(qs.parse(window.location.search.replace(/^\?/, '')).from))
                        }
                    )}>login</button> :
                    <button onClick={() => logout()}>logout</button>}
            </div>
        )
    }
}