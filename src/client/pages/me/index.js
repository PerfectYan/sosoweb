import React from 'react';
import BaseComponent from '../../components/base_component';

export default class Me extends BaseComponent {

    static loginRequired = true;

    constructor(props) {
        super(props);
    }

    renderContent() {
        return <div>Me Page login required</div>
    }
}