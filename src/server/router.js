import 'babel-polyfill';
import KoaRouter from 'koa-router';
import fs from 'fs';
import util from 'util';
import path from 'path';
import { StaticRouter, matchPath } from 'react-router-dom';
import React from 'react';
import routes from '../share/routes';
import App from '../share/app';
import { renderToString } from 'react-dom/server';
import serialize from 'serialize-javascript';
import { get } from 'lodash';
import fetch from 'isomorphic-fetch';


const router = new KoaRouter();
const readFile = util.promisify(fs.readFile);

router.get('/api/*', async (ctx, next) => {
    const url = 'http://www.hostedredmine.com/' + ctx.request.url.replace(/^\/api\//, '');
    const data = await fetch(url).then(res => res.json());
    ctx.body = {
        code: 0,
        data: data
    }
});

router.get('*', async (ctx, next) => {
    const { request } = ctx;
    const route = routes.find(r => matchPath(request.path, r));
    const prefetch = get(route, 'component.prototype.prefetch');
    let prefetchData = null;

    if (typeof prefetch === 'function') {
        const startTime = Date.now();
        prefetchData = await prefetch(request.query); //prefetch函数必须返回一个promise对象
        console.log(ctx.request.url +'2 ' + (Date.now() - startTime) + 'ms');
    }

    const context = { prefetchData };
    const app = renderToString(
        <StaticRouter location={request.url} context={context}>
            <App/>
        </StaticRouter>
    );

    ctx.body = (await readFile(path.resolve(__dirname, 'index.template.html'))).toString()
        .replace('<!--app-->', app)
        .replace('<!--__PREFETCH_DATA__-->', `<script>window.__PREFETCH_DATA__ = ${serialize(context.prefetchData)}</script>`);
});

export default router;