import fetch from 'isomorphic-fetch';

export function get(url) {
    return fetch(`http://localhost:3000` + url).then(res => res.json());
}

export function getProjects() {
    return get('/api/projects.json')
}

export function getIssues() {
    return get('/api/issues.json')
}