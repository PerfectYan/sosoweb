import React from 'react';
import Footer from '../client/components/footer';
import { Route, Switch, Redirect } from 'react-router-dom';
import routes from './routes';
import { isLogin } from '../utils/user';

const LoginRequiredRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(props) => {
        if (isLogin()) {
            return <Component {...props}/>
        } else {
            return <Redirect to={{
                pathname: '/login',
                search: `?from=${encodeURIComponent(`${window.location.pathname}${window.location.search}`)}`,
                state: { from: props.location }
            }}/>
        }
    }
    }/>
}

export default class App extends React.Component {
    render() {
        return <div>
            <Switch>
                {
                    routes.map(route => {
                        if (route.component.loginRequired) {
                            return <LoginRequiredRoute key={route.path} {...route}/>
                        } else {
                            return <Route key={route.path} {...route}/>
                        }
                    })
                }
            </Switch>
            <Footer/>
        </div>
    }
}
