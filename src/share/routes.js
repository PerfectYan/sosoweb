import Home from '../client/pages/home';
import About from '../client/pages/about';
import Error500 from '../client/pages/500';
import Error404 from '../client/pages/404';
import Me from '../client/pages/me';
import Login from '../client/pages/login';

const routes = [{
    path: '/',
    exact: true,
    component: Home
}, {
    path: '/about',
    exact: true,
    component: About
}, {
    path: '/500',
    exact: true,
    component: Error500
}, {
    path: '/me',
    exact: true,
    component: Me
}, {
    path: '/login',
    exact: true,
    component: Login
}, {
    path: '*',
    component: Error404
}];

export default routes;