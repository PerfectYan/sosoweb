let loginStatus = false;

export function isLogin() {
    return loginStatus;
}

export function login() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            loginStatus = true;
            resolve();
        }, 1000);
    })
}

export function logout() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            loginStatus = false;
            resolve();
        }, 1000);
    });
}