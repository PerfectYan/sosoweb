const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        main: './src/client/index.js',
        vendor: ['react', 'react-dom']
    },
    output: {
        publicPath: '/',
        path: path.resolve('build'),
        filename: '[name]-[chunkhash].js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react', 'stage-1'],
                        plugins: [[
                            "babel-plugin-react-css-modules",
                            {
                               "filetypes":{
                                  ".scss":{
                                     "syntax":"postcss-scss"
                                  }
                               },
                               "generateScopedName":"[local]_[hash:base64:5]"
                            }
                         ]]
                    }
                }
            },
            {
                test: /\.s?css$/,
                use: ExtractTextWebpackPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                minimize: true,
                                localIdentName: "[local]_[hash:base64:5]"
                            }
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
        ]
    },
    plugins: [
        new ExtractTextWebpackPlugin('[name]-[chunkhash].css'),
        new HtmlWebpackPlugin({
            title: 'sosoweb',
            template: path.resolve(__dirname, 'src', 'index.template.html'),
            filename: 'index.template.html'
        }),
    ]
}