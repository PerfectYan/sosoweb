const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './src/server/index.js',
    output: {
        path: path.resolve('build'),
        filename: 'server.js'
    },
    node: {
        __dirname: false,
        __filename: false
    },
    target: 'node',
    externals: [webpackNodeExternals()],
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react', 'stage-1'],
                        plugins: [[
                            "babel-plugin-react-css-modules",
                            {
                                "filetypes":{
                                    ".scss":{
                                        "syntax":"postcss-scss"
                                    }
                                },
                                "generateScopedName":"[local]_[hash:base64:5]"
                            }
                        ]]
                    }
                }
            },
            {
                test: /\.s?css$/,
                use: ExtractTextWebpackPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                minimize: true,
                                localIdentName: "[local]_[hash:base64:5]"
                            }
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
        ]
    },
    plugins: [
        new ExtractTextWebpackPlugin('server-no-use.css')
    ]
}